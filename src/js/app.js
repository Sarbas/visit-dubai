require('./bootstrap');

const Swiper = require('swiper/dist/js/swiper');


$(document).ready(function() {

	// Слайдер 1
	var detalSwiper_1 = new Swiper ('.slider_1', {
	    autoplay: {
		    delay: 4000,
		},
	    autoplayDisableOnInteraction: true,
	    speed: 800,
	    loop: true,
	    navigation: {
            nextEl: '.slider_1 .swiper-button-next',
            prevEl: '.slider_1 .swiper-button-prev',
        },
	});

	// Слайдер 2
	var detalSwiper_2 = new Swiper ('.slider_2', {
		autoplay: {
			delay: 4000,
		},
		slidesPerView: 3,
		spaceBetween: 30,
		autoplayDisableOnInteraction: true,
		speed: 800,
		loop: true,
		pagination: {
			el: '.slider_with_pagination .swiper-pagination',
			type: 'bullets',
			clickable: true,
		},
	});

});