<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dubai</title>

    <link href="/dist/css/style.css" rel="stylesheet">
    <link href="/dist/css/jquery.fancybox.css" rel="stylesheet">

</head>
<body>

    <header>
        <div class="cont">
            <div class="row">
                <div class="left">
                    <div class="logo"></div>
                    <div class="menu">
                        <ul>
                            <li><a class="link" href="#why-dubai">Почему Дубаи</a></li>
                            <li><a class="link" href="#relaxation">Отдых</a></li>
                            <li><a class="link" href="#hotels">Рекомендуемые отели</a></li>
                        </ul>
                    </div>
                </div>
                <div class="right">
                    <a class="link but" href="#tour">Запланировать поездку</a>
                    <div class="contacts">
                        <a class="link" href="tel:+77788701072">+7 778 870–10–72</a>
                        <a class="link" href="tel:+77273468083">+7 (727) 346–80–83</a>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div class="section_slider_1">
        <div class="swiper-container slider_1">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <div class="img" style="background-image: url(/images/slide-min.jpg);">
                        <div class="slide_block">
                            <div class="cont">
                                <div class="row">
                                    <div class="column">
                                        <div class="title">Откройте для себя город возможностей</div>
                                        <div class="tag_row">
                                            <div class="tag pink">
                                                <div class="icon"></div>
                                                <div class="text">впечатления</div>
                                            </div>
                                            <div class="tag blue">
                                                <div class="icon"></div>
                                                <div class="text">отдых</div>
                                            </div>
                                            <div class="tag green">
                                                <div class="icon"></div>
                                                <div class="text">развлечения</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="img" style="background-image: url(/images/slide_2-min.jpg);">
                        <div class="slide_block">
                            <div class="cont">
                                <div class="row">
                                    <div class="column">
                                        <div class="title">Лето круглый<br />год в Дубае</div>
                                        <div class="tag_row">
                                            <div class="tag pink">
                                                <div class="icon"></div>
                                                <div class="text">Спланируйте идеальные семейные каникулы</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="img" style="background-image: url(/images/slide_3-min.jpg);">
                        <div class="slide_block">
                            <div class="cont">
                                <div class="row">
                                    <div class="column">
                                        <div class="title">Лето круглый<br />год в Дубае</div>
                                        <div class="tag_row">
                                            <div class="tag blue">
                                                <div class="icon"></div>
                                                <div class="text">Спланируйте идеальный романтический отпуск</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="img" style="background-image: url(/images/slide_4-min.jpg);">
                        <div class="slide_block">
                            <div class="cont">
                                <div class="row">
                                    <div class="column">
                                        <div class="title">Зачем ждать?</div>
                                        <div class="tag_row">
                                            <div class="tag green">
                                                <div class="icon"></div>
                                                <div class="text">Лето всего в нескольких часах пути</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="level_2">
                <div class="cont">
                    <div class="row">
                        <div class="column">
                            <div class="button_row">
                                <div class="swiper-button swiper-button-prev">
                                    <div class="block"></div>
                                </div>
                                <div class="swiper-button swiper-button-next">
                                    <div class="block"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section_video">
        <a href="https://www.youtube.com/embed/AgaPSqoLcQA?autoplay=1" class="play fancybox fancybox.iframe"></a>
    </div>

    <div class="section_steps_head" id="why-dubai">
        <div class="cont">
            <div class="title_1">Как провести время<br />в Дубае</div>
            <div class="title_2">От Бурдж Халифа до Пальма Джумейра: главные символы Дубая.</div>
            <div class="text">Дубай — это город чудес, в которые можно поверить только увидев их своими глазами. Это город, где шедевры современной архитектуры
                соседствуют с историческими кварталами, а в море вырастают рукотворные острова. Мы расскажем вам о главных достопримечательностях,
                которые должен посетить каждый, кто приезжает в Дубай.</div>
        </div>
    </div>

    <div class="section_step step_1 pink">
        <div class="cont">
            <div class="row">
                <div class="col_big">
                    <div class="img" style="background-image: url(/images/step_1.jpg);"></div>
                    <div class="number_block"><div class="number">1</div></div>
                </div>
                <div class="col_small">
                    <div class="text_block">
                        <div class="with_title">
                            <div class="title_1">Бурдж Халифа</div>
                            <div class="title_2">Символ Дубая</div>
                        </div>
                        <div class="block">
                            <div class="text">
                                Небоскреб Бурдж Халифа, вознесшийся на высоту 828 метров, не заметить невозможно.
                                Самое высокое здание в мире видно со всех точек города, но истинное величие этой
                                громады можно почувствовать только вблизи, а еще лучше – внутри небоскреба. В ясный
                                день со смотровой площадки на 124-м этаже открывается совершенно потрясающая панорама.
                                Превзойти ее может, пожалуй, только вид из роскошного лаунжа At The Top Sky Lounge на
                                148-м этаже. А если вы хотите ощутить себя небожителем и устроить ужин в облаках,
                                загляните в ресторан At.mosphere на 122-м этаже.
                            </div>
                        </div>
                        <div class="str"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section_step step_2 step_reverse blue">
        <div class="cont">
            <div class="row">
                <div class="col_big">
                    <div class="img" style="background-image: url(/images/step_2.jpg);"></div>
                    <div class="number_block"><div class="number">2</div></div>
                </div>
                <div class="col_small">
                    <div class="text_block">
                        <div class="with_title">
                            <div class="title_1">The Dubai Mall</div>
                            <div class="title_2">Незабываемый<br />опыт шопинга</div>
                        </div>
                        <div class="block">
                            <div class="text">
                                Прямо по соседству с небоскребом Burj Khalifa расположен торговый центр Dubai Mall.
                                Впрочем, назвать этот громадный коммерческо-развлекательный комплекс обычным торговым
                                центром, будет пеступлением против истины. Вам не хватит и целого дня, чтобы посетить
                                все его бутики и попробовать его развлекательные предложения. Здесь расположены более
                                1200 и более 150 ресторанов, здесь же находится крытый тематический парк, ледовый каток,
                                огромный водопад и главная достопримечательность - Dubai Aquarium and Underwater Zoo.
                            </div>
                         </div>
                        <div class="str"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section_step step_3 green">
        <div class="cont">
            <div class="row">
                <div class="col_big">
                    <div class="img" style="background-image: url(/images/step_3.jpg);"></div>
                    <div class="number_block"><div class="number">3</div></div>
                </div>
                <div class="col_small">
                    <div class="text_block">
                        <div class="with_title">
                            <div class="title_1">Фонтан Дубай</div>
                            <div class="title_2">Танец воды</div>
                        </div>
                        <div class="block">
                            <div class="text">
                                Фонтан Дубай, расположенный между легендарным небоскребом Бурдж Халифа и знаменитым
                                торговым центром Dubai Mall – это самый большой поющий фонтан в мире, выбрасывающий
                                струи воды на высоту до 150 метров. Это одно из самых популярных шоу в Дубае, во время
                                которого переливающиеся всеми цветами радуги струи воды попеременно взмывают ввысь в
                                такт популярным мелодиям со всего мира. Ежедневно проводятся два дневных представления
                                (13:00 и 13:30), кроме пятницы (с 13:30 до 14:00), а вечером шоу начинается на закате
                                и повторяется каждые 30 минут до 23:00.
                            </div>
                        </div>
                        <div class="str"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section_step step_4 step_reverse pink">
        <div class="cont">
            <div class="row">
                <div class="col_big">
                    <div class="img" style="background-image: url(/images/step_4.jpg);"></div>
                    <div class="number_block"><div class="number">4</div></div>
                </div>
                <div class="col_small">
                    <div class="text_block">
                        <div class="with_title">
                            <div class="title_1">Пальма<br />Джумейра</div>
                            <div class="title_2">Остров в форме<br />пальмы</div>
                        </div>
                        <div class="block">
                            <div class="text">
                                Этот рукотворный остров в форме пальмового дерева по праву называют визитной карточкой
                                Дубая. Пальма Джумейра, воплощенный гений инженерной мысли — один из крупнейших
                                искусственных островов в мире. Фешенебельные отели Пальмы, среди которых Waldorf
                                Astoria, Fairmont, One&Only, Jumeirah Zabeel Saray и, конечно же, легендарный курорт
                                Atlantis The Palm – любимое место отдыха не только у туристов, но и местных жителей.
                                Сюда очень легко добраться по монорельсу, соединяющему трамвайную линию города со
                                "стволом" острова.
                            </div>
                        </div>
                        <div class="str"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section_step step_5 blue">
        <div class="cont">
            <div class="row">
                <div class="col_big">
                    <div class="img" style="background-image: url(/images/step_5.jpg);"></div>
                    <div class="number_block"><div class="number">5</div></div>
                </div>
                <div class="col_small">
                    <div class="text_block">
                        <div class="with_title">
                            <div class="title_1">Дубайская<br />бухта Крик</div>
                            <div class="title_2">Откройте для себя<br />исторический Дубай</div>
                        </div>
                        <div class="block">
                            <div class="text">
                                Современный Дубай ассоциируется с величием и блеском своих головокружительных
                                небоскребов, но настоящим сердцем города всегда была, есть и будет бухта Дубай-Крик.
                                Именно здесь, по берегам морского рукава, поселилось племя бани-яс. Воды бухты долгое
                                время приносили Дубаю его некогда главные источники дохода: жемчуг и рыболовство.
                                В наши дни этот район хранит историю Дубая. Здесь находится Музей Дубая и традиционные
                                восточные базары: рынки золота, специй и тканей. Если вы окажетесь в этом районе,
                                обязательно прокатитесь на традиционной лодке абра. Это самое дешевое (всего 1 дирхам)
                                и одно из самых ярких туристических развлечений в городе.
                            </div>
                        </div>
                        <div class="str"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section_step step_6 step_reverse green">
        <div class="cont">
            <div class="row">
                <div class="col_big">
                    <div class="img" style="background-image: url(/images/step_6.jpg);"></div>
                    <div class="number_block"><div class="number">6</div></div>
                </div>
                <div class="col_small">
                    <div class="text_block">
                        <div class="with_title">
                            <div class="title_1">The Walk<br />в районе JBR</div>
                            <div class="title_2">Еда. Шопинг. Пляж.</div>
                        </div>
                        <div class="block">
                            <div class="text">
                                Торговый бульвар The Beach, расположенный рядом с районом JBR – это магазины, рестораны,
                                кинотеатры и пляж. И все в одном месте. От концертов приглашенных диджеев по выходным
                                до летних кинотеатров и аквапарка, где можно отдохнуть с детьми – в районе JBR всегда
                                есть чем заняться.
                            </div>
                        </div>
                        <div class="str"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section_step step_7 pink">
        <div class="cont">
            <div class="row">
                <div class="col_big">
                    <div class="img" style="background-image: url(/images/step_7.jpg);"></div>
                    <div class="number_block"><div class="number">7</div></div>
                </div>
                <div class="col_small">
                    <div class="text_block">
                        <div class="with_title">
                            <div class="title_1">Пляж Kite Beach</div>
                            <div class="title_2">Лучший общественный<br />пляж Дубая</div>
                        </div>
                        <div class="block">
                            <div class="text">
                                Kite Beach — это центр водного спорта и развлечений, но здесь также есть чем заняться
                                и на берегу. Загляните в одно из множества кафе и ресторанчиков с прекрасным видом на
                                пляж, понаблюдайте за трюками скейтеров в новеньком скейт-парке или сыграйте в волейбол.
                                Огромный выбор пляжных развлечений плюс потрясающий вид наБурдж Аль Араб – все это делает
                                Kite Beach идеальным местом для отдыха. Ваш день пролетит незаметно.
                            </div>
                        </div>
                        <div class="str"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section_step step_8 step_reverse blue">
        <div class="cont">
            <div class="row">
                <div class="col_big">
                    <div class="img" style="background-image: url(/images/step_8.jpg);"></div>
                    <div class="number_block"><div class="number">8</div></div>
                </div>
                <div class="col_small">
                    <div class="text_block">
                        <div class="with_title">
                            <div class="title_1">Дубай Марина</div>
                            <div class="title_2">Потрясающая<br />панорама города</div>
                        </div>
                        <div class="block">
                            <div class="text">
                                Самая большая в мире рукотворная гавань для яхт – Дубай Марина. Полюбуйтесь этим
                                архитектурным чудом, прогуливаясь по променаду Dubai Marina Walk или взяв яхтенный
                                круиз в яхт-клубе Dubai Marina Yacht Club.
                            </div>
                        </div>
                        <div class="str"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section_step step_9 green">
        <div class="cont">
            <div class="row">
                <div class="col_big">
                    <div class="img" style="background-image: url(/images/step_9.jpg);"></div>
                    <div class="number_block"><div class="number">9</div></div>
                </div>
                <div class="col_small">
                    <div class="text_block">
                        <div class="with_title">
                            <div class="title_1">Пустыня</div>
                            <div class="title_2">Величественные<br />пейзажи</div>
                        </div>
                        <div class="block">
                            <div class="text">
                                Всего в 20 минутах езды от суперсовременного центра Дубая раскинулась волшебная
                                страна – Аравийская пустыня. Это одна из самых популярных туристических
                                достопримечательностей Дубая. Сафари в пустыню в дубайском стиле — это джиппинг по
                                дюнам, катание на квадроциклах и сэндбординг днем, и прогулки на верблюдах, роспись
                                хной и традиционный ужин-барбекю вечером. Вы также можете взять автомобиль напрокат и
                                самостоятельно попутешествовать по пустыне. Если вы привыкли к роскоши, сафари Heritage
                                Dinner Safari создано специально для вас. Недостаточно одного дня? Проведите незабываемую
                                ночь в пустыне на спа-курортах Al Maha Desert Resort & Spa или Bab Al Shams Desert Resort & Spa.
                            </div>
                        </div>
                        <div class="str"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section_step step_10 step_reverse pink">
        <div class="cont">
            <div class="row">
                <div class="col_big">
                    <div class="img" style="background-image: url(/images/step_10.jpg);"></div>
                    <div class="number_block"><div class="number">10</div></div>
                </div>
                <div class="col_small">
                    <div class="text_block">
                        <div class="with_title">
                            <div class="title_1">Исторический<br />район<br />Аль-Фахиди</div>
                            <div class="title_2">Откройте для себя<br />историю Дубая</div>
                        </div>
                        <div class="block">
                            <div class="text">
                                Исторический район Аль-Фахиди в Бар-Дубай — это один из старейших районов города.
                                Ветряные башни, лабиринты узких улочек — все здесь хранит на себе отпечаток прошедших
                                времен, поэтому пешая прогулка по этому району превращается в путешествие во времени.
                                Посетите Центр культурного сотрудничества шейха Мохаммеда (SMCCU), который предлагает
                                занятия по арабскому языку, проводит культурные туры и экскурсии в мечети, помогающие
                                лучше понять местные обычаи и традиции. Если вы хотите продолжить знакомство с местной
                                культурой, переправьтесь на традиционной лодке абра на другой берег бухты Крик, где
                                находятся восточные базары.
                            </div>
                            </div>
                        <div class="str"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section_info" id="relaxation">
        <div class="img" style="background-image: url(/images/parks.jpg);">
            <div class="cont">
                <div class="wrap">
                    <div class="column">
                        <div class="title">Все тематические<br />парки Дубая</div>
                        <div class="tag blue">
                            <div class="icon"></div>
                            <div class="text">Мир приключений ждет вас</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section_video">
        <a href="https://www.youtube.com/embed/sYRcf7eXPco?autoplay=1" class="play fancybox fancybox.iframe"></a>
    </div>

    <div class="section_parks">
        <div class="cont">
            <div class="title">Тематические парки Дубая</div>
            <div class="slider_with_pagination">
                <div class="swiper-container slider_2">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="block">
                                <div class="block_in">
                                    <div class="logo" style="background-image: url(/images/park_logo_1.png);"></div>
                                    <a class="img fancybox fancybox.iframe section_video" href="https://www.youtube.com/embed/LiY5Qp6kRXg?autoplay=1"
                                       style="background-image: url(/images/park_img_1.jpg);"></a>
                                    <div class="text">Motiongate Dubai знакомит посетителей со знаменитыми героями голливудских блокбастеров.</div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="block">
                                <div class="block_in">
                                    <div class="logo" style="background-image: url(/images/park_logo_2.png);"></div>
                                    <a class="img fancybox fancybox.iframe section_video" href="https://www.youtube.com/embed/ZFNh1xoh3Hs?autoplay=1"
                                       style="background-image: url(/images/park_img_2.jpg);"></a>
                                    <div class="text">Парк - мечта для детей в возросте от 2-х до 12-ти лет. Посетите с ними главные аттракционы и горки парка LEGOLAND Dubai.</div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="block">
                                <div class="block_in">
                                    <div class="logo" style="background-image: url(/images/park_logo_3.png);"></div>
                                    <a class="img fancybox fancybox.iframe section_video" href="https://www.youtube.com/embed/6sxUuVaBcVA?autoplay=1"
                                       style="background-image: url(/images/park_img_3.jpg);"></a>
                                    <div class="text">Территория площадью равной 28 футбольным полям - в парке IMG Worlds of Adventure вам будет где разгуляться.</div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="block">
                                <div class="block_in">
                                    <div class="logo" style="background-image: url(/images/park_logo_4.png);"></div>
                                    <a class="img fancybox fancybox.iframe section_video" href="https://www.youtube.com/embed/efhAlaM1eq0?autoplay=1"
                                       style="background-image: url(/images/park_img_4.jpg);"></a>
                                    <div class="text">Посетите первый в мире тематический парк, посвященный Болливуду и получите заряд бодрости в этом царстве зажигательных танцев и музыки.</div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="block">
                                <div class="block_in">
                                    <div class="logo" style="background-image: url(/images/park_logo_5.png);"></div>
                                    <a class="img fancybox fancybox.iframe section_video" href="https://www.youtube.com/embed/Xb1ZKYlsFV4?autoplay=1"
                                       style="background-image: url(/images/park_img_5.jpg);"></a>
                                    <div class="text">Аттракционы и горки парка Legoland Water Park расчитаны на детей от 2-х до 12-ти лет.</div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="block">
                                <div class="block_in">
                                    <div class="logo" style="background-image: url(/images/park_logo_6.png);"></div>
                                    <a class="img fancybox fancybox.iframe section_video" href="https://www.youtube.com/embed/Y6lhz6RKbNI?autoplay=1"
                                       style="background-image: url(/images/park_img_6.jpg);"></a>
                                    <div class="text">Laguna Waterpark в прибрежном районе La Mer -  новое, популярное место для семейного досуга.</div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="block">
                                <div class="block_in">
                                    <div class="logo" style="background-image: url(/images/park_logo_7.png);"></div>
                                    <a class="img fancybox fancybox.iframe section_video" href="https://www.youtube.com/embed/oWokmVTot8k?autoplay=1"
                                       style="background-image: url(/images/park_img_7.jpg);"></a>
                                    <div class="text">В аквапарке Wild Wadi Waterpark вам будут рады круглый год, ведь в Дубае тепло и солнечно 365 дней в году.</div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="block">
                                <div class="block_in">
                                    <div class="logo" style="background-image: url(/images/park_logo_8.png);"></div>
                                    <a class="img fancybox fancybox.iframe section_video" href="https://www.youtube.com/embed/8vvItDOVPmE?autoplay=1"
                                       style="background-image: url(/images/park_img_8.jpg);"></a>
                                    <div class="text">Aquaventure - один из самых знаменитых аквапарков Дубая, здесь находится самая длинная горка на Ближнем Востоке.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-pagination"></div>
            </div>
        </div>
    </div>

    <div class="section_info">
        <div class="img" style="background-image: url(/images/shoping.jpg);">
            <div class="cont">
                <div class="wrap">
                    <div class="column">
                        <div class="title">Шопинг<br />в Дубае</div>
                        <div class="block blue">
                            <div class="text">
                                Дубай – это грандиозные торговые центры,
                                традиционные восточные базары и модные бутики.
                                Еще никто не уезжал из нашего города без покупок и
                                оригинальных сувениров! Наши невероятные
                                шопинг-фестивали, такие как Дубайский торговый
                                фестиваль и Dubai Summer Surprises, дают вам
                                возможность купить то, о чем вы давно мечтали,
                                с фантастической скидкой!
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section_info">
        <div class="img" style="background-image: url(/images/beach.jpg);">
            <div class="cont">
                <div class="wrap">
                    <div class="column">
                        <div class="title">Пляж в Дубае</div>
                        <div class="block pink">
                            <div class="text">
                                <p>Устройте для себя пляжный отдых на знаменитом
                                    побережье Дубая. Кристально чистая вода, золотой
                                    песок и несметное число пляжных кафе и
                                    ресторанов – что еще нужно для идеального
                                    пляжного отдыха.</p>
                                <p>Посетите один из общественных пляжей в Дубае
                                    и познакомьтесь с особенностями арабской пляжной
                                    жизни или проведите вечер в одном из
                                    фешенебельных пляжных клубов, разбросанных по
                                    всему побережью. Выбор клубов огромен, от
                                    семейных вариантов до заведений, где будет весело
                                    компании друзей.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section_recomended_hotels" id="hotels">
        <div class="cont">
            <div class="title">Рекомендуемые отели</div>
            <div class="row">
                <div class="col">
                    <div class="block blue">
                        <ul>
                            <li>Nihal Hotel 3*</li>
                            <li>Orchid Hotel 3*</li>
                            <li>Ibis Deira City Centre 2*</li>
                            <li>Golden Tulip Nihal Palace Hotel 4*</li>
                            <li> Flora Grand Hotel 4*</li>
                            <li>Cassells Al Barsha Hotel 4*</li>
                            <li>Signature 1 Hotel Tecom 4* </li>
                        </ul>
                    </div>
                </div>
                <div class="col">
                    <div class="block pink">
                        <ul>
                            <li>Flora Inn Hotel 4*</li>
                            <li>Grand Hyatt Hotel 5*</li>
                            <li>Le Meridien Mina Seyahi Beach
                                Resort & Marina 5*
                            </li>
                            <li>Movenpick Hotel Jumeirah Beach 5*</li>
                            <li>Lapita Dubai Parks And Resorts 5*</li>
                            <li>Sheraton Jumeirah Beach Resort 5*</li>
                        </ul>
                    </div>
                </div>
                <div class="col">
                    <div class="block green">
                        <ul>
                            <li>Swissotel Al Ghurair Dubai 5*</li>
                            <li>Hilton Dubai Jumeirah Beach 5* </li>
                            <li>Fairmont The Palm 5*</li>
                            <li>Rixos The Palm Dubai Hotel
                                And Suites 5* </li>
                            <li>Rixos Premium Dubai 5*</li>
                            <li>Atlantis The Palm Dubai 5*</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section_feedback" id="tour">
        <div class="cont">
            <a href="https://kaztour.kz/" class="tours"><div>Найти тур</div></a>
            <div class="title">или</div>
            <div class="title">оставьте заявку</div>
            <form action="#" id="feedback">
                <div class="row">
                    <div class="col">
                        <input type="text" name="name" placeholder="Имя" />
                    </div>
                    <div class="col">
                        <input type="text" name="email" placeholder="Email" />
                    </div>
                    <div class="col">
                        <input type="text" name="phone" placeholder="Телефон" />
                    </div>
                    <div class="col">
                        <button type="submit">Оставить заявку</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <footer>
        <div class="cont">
            <div class="row">
                <div class="left">
                    <div class="logo"></div>
                    <div class="with_soc">
                        <div class="text">Мы в соц. сетях:</div>
                        <div class="soc_row">
                            <a href="#" class="soc fb"></a>
                            <a href="#" class="soc inst"></a>
                            <a href="#" class="soc youtube"></a>
                            <a href="#" class="soc tel"></a>
                        </div>
                    </div>
                </div>
                <div class="right">
                    <a class="link but" href="#tour">Запланировать поездку</a>
                    <div class="contacts">
                        <a class="link" href="#">+7 778 870–10–72</a>
                        <a class="link" href="#">+7 (727) 346–80–83</a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    
    <script type="text/javascript" src="/dist/js/app.js"></script>
    <script type="text/javascript" src="/dist/js/jquery.fancybox.pack.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $(".fancybox").fancybox();
        });
    </script>
</body>
</html>